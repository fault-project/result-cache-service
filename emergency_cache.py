from collections import OrderedDict
from platform import machine
import requests
import json
import time
import pymongo
from pymongo import MongoClient


class FetchEmergencyAlerts:
    def __init__(self):
        self.backend_uri = "http://backend:5000/"
    def fetch_service_details_from_backend(self, machine_id):
        service_details = requests.get(self.backend_uri + "web/fetch/device/alarm" + "?uuid=" + machine_id)
        return self.process_services_api_response(service_details=service_details)
    def process_services_api_response(self, service_details):
        if service_details.text == "-1":
            print("Response -1")
            return -1
        q = json.loads(service_details.text)
        print(type(q[0]))
        print(f"Api response => ", q)
        return json.loads(service_details.text)
    def make_bot_alarm_call(self, statement: str, machine_id: str):
        print("Sending mails")
        resp = requests.get(self.backend_uri + "web/alarm/bot/make-call?device_uuid=" + machine_id + "&message_value=" + statement)
        return resp
    def make_email_alarm_call(self, statement : str, machine_id : str):
        resp = requests.get(self.backend_uri + "web/alarm/email/make-call?device_uuid=" + machine_id + "&message_value=" + statement)
        return resp



# If settings are changed from the frontend while the queue is working, the entire cache will need to be reset
class Machine_Data_Pool:
    def __init__(self, machine_id, bot_alarm_status, email_alarm_status):
        self.machine_id = machine_id
        self.bot_alarm_status = bot_alarm_status
        self.email_alarm_status = email_alarm_status

    def fetch_details(self):
        return [self.bot_alarm_status, self.email_alarm_status]

class Easy_cache:
    def __init__(self):
        self.cache_map = {}
    def get(self, machine_id, log_value):
        print("inside get " + machine_id)
        if machine_id in self.cache_map:
            machine_obj = self.cache_map[machine_id]
            machine_service_list = machine_obj.fetch_details()
        else:
            fetch_emergency_code = FetchEmergencyAlerts()
            machine_service_list = fetch_emergency_code.fetch_service_details_from_backend(machine_id=machine_id)
            if machine_service_list == -1:
                return -1
            else:
                new_machine_obj = Machine_Data_Pool(bot_alarm_status=machine_service_list[0], email_alarm_status=machine_service_list[1], machine_id=machine_id)
                self.put(machine_id=machine_id, value=new_machine_obj)

        if machine_service_list[0] == True:
            print("True for bot")
            fetch_emergency_code.make_bot_alarm_call(statement=log_value, machine_id=machine_id)
        if machine_service_list[1] == True:
            print("True for Email")
            fetch_emergency_code.make_email_alarm_call(statement=log_value, machine_id=machine_id)
        
    
    def put(self, machine_id, value : Machine_Data_Pool):
        self.cache_map[machine_id] = value


# A specific DB Storage where only the logs with their respective errors are stored
class Dump_Log_To_DB:
    
    def upload_log_to_db(self, machine_id, log_value):
        cluster = MongoClient("mongodb+srv://team86:team86@early-fault-detection.cnjve.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
        db = cluster["logs"]
        log_doc = db["error_logs"]
        log_doc.insert_one({"log-val" : log_value, "time" : time.time(), "m-id" : machine_id})
    
    


# ob1 = Dump_Log_To_DB()
# ob1.upload_log_to_db("temp2", "temp100021212")
# class Lru:  
#     def __init__(self, capacity):
#         self.capacity = capacity
#         self.cache_map = OrderedDict()
    
#     def get(self, key):
#         if key in self.cache_map:
#             self.cache_map.move_to_end(key)
#             machine_obj = self.cache_map[key]
#             return machine_obj.fetch_data()
#         else:
#             return -1
    
#     def put(self, key):
#         pass

            



# obj1 = FetchEmergencyAlerts()

# # obj1.fetch_service_details_from_backend("random-uuid")
# obj1.make_email_alarm_call("temp_statement", "temp_id")



# obj2 = Easy_cache()
# obj2.get(machine_id="random-uuid", log_value="random log vlaue")