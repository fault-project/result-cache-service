from query_checker import Query_Checker
import json
import sqlite3

class Api_Query:
    statement_dict = {
        "machine_id" : "MachineID = ",
        "remediation_action" : "remediation = ",
        "start_time" : "startTime = ",
        "end_time" : "endTime = ", 
        "creation_time" : "creation_time = ",
        "query_limit" : "limit = "
    }
    arg_dict = None

    def __init__(self, arg_dict):
        self.arg_dict = arg_dict


    def query_maker(self) -> str:
        base_query = "SELECT * FROM CacheLogs WHERE "
        additional_query = ""
        query_params = list(self.arg_dict.keys())
        print(f"query params => {query_params}")
        
        obj1 = Query_Checker()
        where_stat_add = obj1.query_transform(self.arg_dict)

        print(f"where_stat_add => {where_stat_add}")

        if "start_time" in query_params:
            query_params.remove("start_time")
        if "end_time" in query_params:
            query_params.remove("end_time")

        for k_ind in range(len(query_params)):
            val1 = self.statement_dict[query_params[k_ind]]
            val2 =  self.arg_dict[query_params[k_ind]]
            additional_query = additional_query + val1 + str(val2)
            if k_ind != len(query_params) - 1:
                additional_query += " AND "
            # else:
                # additional_query += ";"
        base_query += additional_query
        base_query += where_stat_add
        print(base_query)
        return base_query


    def query_check_and_fetch(self) -> json:
        obj1 = Query_Checker()
        is_query_correct = obj1.conditions(self.arg_dict)
        if is_query_correct != True:
            return json.dumps("Wrong Query")
        query_formed = self.query_maker()
        print(f"query formed => {query_formed}")
        resp = self.db_query(query_formed)
        return json.dumps(resp)

    def db_query(self, query):
        try:
            con = sqlite3.connect('cacheDb.db')
            # con.execute('''CREATE TABLE IF NOT EXISTS CacheLogs (ID INTEGER PRIMARY KEY AUTOINCREMENT, MachineID TEXT NOT NULL, CREATIONTIME INTEGER NOT NULL, Errors TEXT, Log TEXT)''')
            resp = con.execute(query)
            print(f"resp => {resp}")         
            for x in resp:
                print(x)
            con.close()
            return list(resp)
        except Exception as e:
            print("exception")
            print(e)
            return str(e)

# obj1 = Api_Query()
# obj1.query_maker