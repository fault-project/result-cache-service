import sqlite3
import time

class Log_Storer:
    def insert_to_db(machine_id, log_val, err_detect) -> None:
        con = sqlite3.connect("cacheDb.db")
        cur = con.cursor()
        # CREATE TABLE IF NOT EXISTS CacheLogs (ID INTEGER PRIMARY KEY AUTOINCREMENT, 
        # MachineID TEXT NOT NULL, CREATIONTIME INTEGER NOT NULL,  Errors TEXT, Log TEXT)
        curr_time = str(time.time())
        con.execute('''INSERT INTO CacheLogs (MachineID, CREATIONTIME, Errors, Log) VALUES (?, ?, ?, ?)''', (machine_id, curr_time, err_detect, log_val,))
        con.commit()
        con.close()
    
