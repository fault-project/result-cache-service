from platform import machine
from flask import Flask, request
import json
from api_query import Api_Query
from Db_Ops import Log_Storer
from emergency_cache import Dump_Log_To_DB, Easy_cache, FetchEmergencyAlerts

cache_api = Flask(__name__)


# class Routes:
#     @cache_api.route("/store")
#     def store_pi():
#         return "hello"
#     @cache_api.route("/new/store")
#     def abcd():
#         return "yo"

# obj1 = Routes()


# @cache_api.route("/store/log-processing/result", methods=['POST'])
# def store_log_processing_result() -> json:
#     req = request.json
#     # machine id : str, log value : str and errors detected : str
#     mach_id = req["machine_id"]
#     log_val = req["log_val"]
#     err_detect = req["err_detect"]
#     Log_Storer.insert_to_db(machine_id= mach_id, log_val= log_val, err_detect= err_detect)
#     return "success"



# @cache_api.route("/fetch/log-processing/cache", methods=['GET'])
# def fetch_cache():
#     req_data = request.args.to_dict()
#     print("type => " , type(req_data), " value ", req_data)
#     obj1 = Api_Query(req_data)
#     formed_query = obj1.query_maker()
#     resp = obj1.query_check_and_fetch()

#     return resp




OK = 'SUCCESS'



@cache_api.route("/store/log-result", methods=['POST'])
def store_log_processing_result():
    global cache_class, db_class
    req = request.json
    machine_id = req['machine_id']
    result = req['result']
    print(f"result -> {result} and machine id {machine_id}")
    fetch_emergency_alarm_Service.make_bot_alarm_call(statement=str(result), machine_id=machine_id)
    fetch_emergency_alarm_Service.make_email_alarm_call(statement=str(result), machine_id=machine_id)
    db_class.upload_log_to_db(machine_id=machine_id, log_value=str(result))
    return OK


@cache_api.route("/refresh/cache-settings", methods=['GET'])
def refresh_cache_settings():
    global cache_class
    new_cache_class = Easy_cache()
    cache_class = new_cache_class
    return OK


if __name__ == '__main__':
    # con = sqlite3.connect('cacheDb.db')
    # con.execute('''CREATE TABLE IF NOT EXISTS CacheLogs (ID INTEGER PRIMARY KEY AUTOINCREMENT, MachineID TEXT NOT NULL, CREATIONTIME INTEGER NOT NULL, Errors TEXT, Log TEXT)''')
    # con.close()
    cache_class = Easy_cache()
    db_class = Dump_Log_To_DB()
    fetch_emergency_alarm_Service = FetchEmergencyAlerts()
    cache_api.run(debug=True, host='0.0.0.0', port = 4999)