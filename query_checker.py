class Query_Checker:
    query_params = None
    def __init__(self):
        # self.query_params = query_params
        pass

    def conditions(self, query_param) -> bool:
        # creation time and end_time/start_time cannot be together in a query
        if "creation_time" in query_param and ("end_time" or "start_time" in query_param):
            return False
        return True
    
    def query_transform(self, query_param) -> str:
        statement1 = ""
        if ("end_time" in query_param) and ("start_time" in query_param):
            statement1 = " AND CREATIONTIME BETWEEN " + query_param["start_time"] + " AND " + query_param["end_time"] + ";"
        elif ("end_time" in query_param):
            statement1 = " AND CREATIONTIME < " + query_param["end_time"] + ";"
        elif ("start_time" in query_param):
            statement1 = " AND CREATIONTIME > " + query_param["start_time"] + ";"
        return statement1

        